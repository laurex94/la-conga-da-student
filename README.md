# INTRODUCCIÓN AL ANÁLISIS DE DATOS. LA-CoNGA

## Bienvenidos!


<img src="https://gitlab.com/bivl2ab/academico/cursos-uis/la-conga-dataanalysis/la-conga-da-student/-/raw/master/imgs/banner_DA.png"  width="1000px" height="200px">


## Colaboratory (Google)

Vamos a utilizar la plataforma de google para editar, compartir y correr notebooks: [**Colaboratory**](https://colab.research.google.com/notebooks/welcome.ipynb)

- Necesitas una cuenta de gmail y luego entras a drive
- Colaboratory es un entorno de notebook de Jupyter gratuito que no requiere configuración y se ejecuta completamente en la nube.
    - Usaremos parte de la infraestructura de computo de google... gratis! (máximo procesos de 8 horas)
- Con Colaboratory, puedes escribir y ejecutar código, guardar y compartir análisis, y acceder a recursos informáticos potentes, todo gratis en tu navegador.
- También puedes usar los recursos de computador Local.


## Talleres (Problemsets)

Los talleres pretenden ser una herramienta practica para afianzar los conocimientos desarrollados durante las clases. En general se presentan como un conjunto de ejercicios que serán desarrollados **individualmente** por los estudiantes. Cada taller esta escrito como un notebook para la validación automática. Se pueden hacer tantos intentos como se quieran y unicamente la última respuesta será tomada en cuenta. Cada uno de los talleres se desarrollará  dentro de las fechas establecidas en el cronograma.



## Calendario y plazos

                        SESSION 1            SESSION 2              SESSION SATURDAY


     W01 Mar02-Mar04    Intro-colab-repos    Python-general
     W02 Mar09-Mar11    Python-Numpy         Pandas
     W03 Mar16-Mar18    Pandas               Visualización
     W04 Mar23-Mar25    análisis datos       análisis de datos
